package com.colorinfoapp.android.helpers;

import junit.framework.TestCase;

public class ColourHelperTest extends TestCase {

    public void testColourFromHexString() {
        assertTrue(ColourHelper.colorFromHex("#FF00FF") == 0xFFFF00FF);
        assertTrue(ColourHelper.colorFromHex("FF00FF") == 0xFFFF00FF);
        assertTrue(ColourHelper.colorFromHex("#01020304") == 0x01020304);
        assertTrue(ColourHelper.colorFromHex("01020304") == 0x01020304);
    }


    public void testHexFromColorInt() {
        assertTrue(ColourHelper.hexFromColor(0xFFFF00FF).equals("#FF00FF"));
        assertTrue(ColourHelper.hexFromColor(0x01020304).equals("#01020304"));
    }




    public void testRGB(){
        assertTrue(ColourHelper.colorFromRgbComponents(new int[]{255,9,9,9}) == 0xFF090909);

        int color = 0xFFFF0506;
        int[] rgbComponents = ColourHelper.rgbComponents(color);
        assertTrue(rgbComponents[0] == 255);
        assertTrue(rgbComponents[1] == 255);
        assertTrue(rgbComponents[2] == 5);
        assertTrue(rgbComponents[3] == 6);

        color = 0x05060708;
        rgbComponents = ColourHelper.rgbComponents(color);
        assertTrue(rgbComponents[0] == 5);
        assertTrue(rgbComponents[1] == 6);
        assertTrue(rgbComponents[2] == 7);
        assertTrue(rgbComponents[3] == 8);

        assertTrue(color == ColourHelper.colorFromRgbComponents(ColourHelper.rgbComponents(color)));
    }


    public void testhsvComponentsFromColor() {
        int color = 0x05060708;
        float [] hsvComponents = ColourHelper.hsvComponentsFromColor(color);
        assertTrue(hsvComponents[0] == 210.0f);
        assertTrue(hsvComponents[1] == 0.25f);
        assertTrue((Math.round(hsvComponents[2]*1000)/1000.0f) == 0.031f);


    }
    public void testcolorFromHsvComponents() {
        float [] hsvComponents = new float[3];
        hsvComponents[0] = 210.0f;
        hsvComponents[1] = 0.25f;
        hsvComponents[2] = 0.03137255f;

        int color = ColourHelper.colorFromHsvComponents(hsvComponents);
        assertTrue(color == 0xFF060708);
    }

    public void testhslComponentsFromColor() {
        int color = 0x05060708;
        float [] hslComponents = ColourHelper.hslComponentsFromColor(color);
        assertTrue(hslComponents[0] == 210.0f);
        assertTrue((Math.round(hslComponents[1]*100)/100.0f) == 0.14f);
        assertTrue((Math.round(hslComponents[2]*100)/100.0f)== 0.03f);


    }

    public void testcolorFromHslComponents() {
        float [] hslComponents = new float[3];
        hslComponents[0] = 210.0f;
        hslComponents[1] = 0.14285718f;
        hslComponents[2] = 0.027450982f;

        int color = ColourHelper.colorFromHslComponents(hslComponents);
        assertTrue(color == 0xFF060708);
    }

    public void testcmykComponentsFromColor() {
        int color = 0x05060708;
        float[] cmykComponents = ColourHelper.cmykComponentsFromColor(color);
        assertTrue(cmykComponents[0] == 0.25f);
        assertTrue(cmykComponents[1] == 0.125f);
        assertTrue(cmykComponents[2]== 0.0f);
        assertTrue((Math.round(cmykComponents[3]*1000)/1000.0f)==0.969f);
    }
    public void testcolorFromCmykComponents() {
        float []cmykComponents = new float[4];
        cmykComponents[0] = 0.25f;
        cmykComponents[1] = 0.125f;
        cmykComponents[2] = 0.0f;
        cmykComponents[3] = 0.969f;
        int color = ColourHelper.colorFromCmykComponents(cmykComponents);
        assertTrue(color==0xFF060708);
    }


    public void testlabComponentsFromColor() {
        int color = 0x05060708;
        double[] labComponents = ColourHelper.labComponentsFromColor(color);
        assertTrue(labComponents[0] == 1.8807371699144042);
        assertTrue(labComponents[1] == -0.12215436968529558);
        assertTrue(labComponents[2] == -0.47116794207296975);

    }
    public void testcolorFromLabComponents() {
        double [] labComponents = new double[3];
        labComponents[0] = 1.8807371699144042;
        labComponents[1] = -0.12215436968529558;
        labComponents[2] = -0.47116794207296975;

        int color = ColourHelper.colorFromLabComponents(labComponents);
        assertTrue(color == 0xFF060708);
    }

}
