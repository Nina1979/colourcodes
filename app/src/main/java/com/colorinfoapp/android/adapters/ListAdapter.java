package com.colorinfoapp.android.adapters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.helpers.ColourHelper;
import com.colorinfoapp.android.models.ColourModel;
import com.colorinfoapp.android.models.PaletteModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by bsshe on 3/16/2016.
 */
public class ListAdapter extends BaseAdapter {

    Context context;
    private ArrayList<ColourModel> data;
    private ArrayList<ColourModel> filteredData = new ArrayList<>();
    int redValue;
    int greenValue;
    int blueValue;
    private LayoutInflater inflater;
    private HashMap<ColourModel, Float> sortMap;
    PaletteModel selectedPalette;


    public ListAdapter(Context context, PaletteModel selectedPalette) {
        this.context = context;
        // getting the colours from selected palette
        this.data = selectedPalette.getColours();
        // adding the colours of the selected palette into filtered data arraylist
        this.filteredData.addAll(this.data);
        this.selectedPalette = selectedPalette;

        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LocalBroadcastManager.getInstance(context).registerReceiver(mReceiver,
                new IntentFilter("selected-color-changed"));


    }

        // registering the receiver
        BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        // getting the selected color from the intent
            int pixel = intent.getIntExtra("selected-color", -1);
            // getting the red component of the selected color
            redValue = Color.red(pixel);
            // getting the green component of the selected color
            greenValue = Color.green(pixel);
            // getting the blue component of the selected color
            blueValue = Color.blue(pixel);
            // refiltering colours
            refilter(pixel);

        }
    };
       // updating selected palette
    public void updateSelectedPalette(PaletteModel selectedPalette){
        this.data = selectedPalette.getColours();
        // clearing filtered data
        this.filteredData.clear();
        this.filteredData.addAll(this.data);
        this.selectedPalette = selectedPalette;
        //this.refilter();
        notifyDataSetChanged();
    }


    public void refilter(int selectedColor) {
        int[] scrgb = ColourHelper.rgbComponents(selectedColor);
        this.getFilteredData().clear();

        sortMap = new HashMap<>();
        for (ColourModel m : this.data) {
            int paletteColor = m.getColor();
            int[] crgb = ColourHelper.rgbComponents(paletteColor);
            float dist = (float) Math.sqrt(Math.pow(scrgb[1] - crgb[1], 2) + Math.pow(scrgb[2] - crgb[2], 2) + Math.pow(scrgb[3] - crgb[3], 2));


//            int redDist = Math.abs(redValue - Color.red(paletteColor));
//            int greenDist = Math.abs(greenValue - Color.green(paletteColor));
//            int blueDist = Math.abs(blueValue - Color.blue(paletteColor));
//            int Sum = redDist + greenDist + blueDist;
//            if (selectedPalette.getPaletteName().equals("Federal Standard")) {
//                if (dist < 70) {
//                    this.getFilteredData().add(m);
//                    sortMap.put(m, dist);
//                }
                    if (dist < 85) {
                        this.getFilteredData().add(m);
                        sortMap.put(m, dist);
                    }
                }



        Collections.sort(this.getFilteredData(), new Comparator<ColourModel>() {
            @Override
            public int compare(ColourModel lhs, ColourModel rhs) {
                float dist1 =  sortMap.get(lhs);
                float dist2 =  sortMap.get(rhs);
                if (dist1 < dist2)
                    return -1;
                else if (dist1 == dist2)
                    return 0;
                else
                    return 1;
            }
        });


        notifyDataSetChanged();
    }



    @Override
    protected void finalize() throws Throwable {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mReceiver);
        super.finalize();
    }
    @Override
    public int getCount() {
        return this.filteredData.size();

    }

    @Override
    public Object getItem(int position) {
        return this.filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
    /* We inflate the xml which gives us a view */
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();

        }


        ColourModel model = (ColourModel) this.getItem(position);

                holder.uiName.setText(model.getColourName());
                holder.uiName.setBackgroundColor(model.getColor());



        return convertView;

    }
    public ArrayList<ColourModel> getFilteredData() {
        return filteredData;
    }

    static class ViewHolder {
        @Bind(R.id.txtlist_item)
        TextView uiName;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }





}
