package com.colorinfoapp.android.adapters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.colorinfoapp.android.R;
import com.colorinfoapp.android.models.ColourData;
import com.colorinfoapp.android.models.PaletteModel;

import java.util.ArrayList;

import static com.colorinfoapp.android.R.color.lightpurple;


/**
 * Created by bsshe on 3/24/2016.
 */
public class SpinnerAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<PaletteModel> palettes;

    public SpinnerAdapter(Context context) {
        this.context = context;
        this.palettes = ColourData.getInstance().getPalettes();

    }

    // vraca broj paleta koje treba da se prikazu u spinneru
    @Override
    public int getCount() {
        return this.palettes.size();
    }
    // vraca element na odredjenoj poziciji iz niza podataka/paleta
    @Override
    public Object getItem(int position) {
        return this.palettes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.spinner_item, parent, false);
            holder = new ViewHolder();
            holder.uiName = (TextView) convertView.findViewById(R.id.spinner_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        PaletteModel model = (PaletteModel) this.getItem(position);
        holder.uiName.setText(model.getPaletteName());
        holder.uiName.setTextColor(lightpurple);
        return convertView;
    }

    static class ViewHolder {
        TextView uiName;
    }
}
