package com.colorinfoapp.android.adapters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.colorinfoapp.android.R;
import com.colorinfoapp.android.models.ColourModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by bsshe on 3/16/2016.
 */
public class ListAdapterTest extends BaseAdapter{

    Context context;
    private ArrayList<ColourModel> data;
    private ArrayList<ColourModel> filteredData = new ArrayList<>();
    int redValue;
    int greenValue;
    int blueValue;
    private LayoutInflater inflater;



    public ListAdapterTest (Context context, ArrayList<ColourModel> data) {
        this.context = context;
        this.data = data;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LocalBroadcastManager.getInstance(context).registerReceiver(mReceiver,
                new IntentFilter("selected-color-changed"));




    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int pixel =intent.getIntExtra("selected-color",-1);

            redValue = Color.red(pixel);
            greenValue = Color.green(pixel);
            blueValue = Color.blue(pixel);
            refilter();

        }
    };

    public void refilter() {
        // 100, 130, 170
        // 100, 135,  80
        //   0,   5,  90 => 95
        this.getFilteredData().clear();

        for (ColourModel m : this.data) {
            int paletteColor = m.getColor();
            int redDist = Math.abs(redValue - Color.red(paletteColor));
            int greenDist = Math.abs(greenValue - Color.green(paletteColor));
            int blueDist = Math.abs(blueValue - Color.blue(paletteColor));
            int Sum = redDist + greenDist + blueDist;
            if (Sum < 100) {
                this.getFilteredData().add(m);
                notifyDataSetChanged();
                sorting();
                notifyDataSetChanged();

            }
        }

//        for (ColourModel m: this.getFilteredData())
//        {
//            Log.d("number of elements",Integer.toString(this.getFilteredData().size()));
//            Log.d("Array Value","Array Value"+ m.toString());
//        }

    }



    public void sorting (){

        Collections.sort(getFilteredData(), new Comparator<ColourModel>() {
                    public int compare(ColourModel model1, ColourModel model2) {
                        if (model1.getColor() > model2.getColor()) {
                            return 1;
                        } else if (model1.getColor() < model2.getColor()){
                            return -1;
                        }
                        else {
                            return 0;
                        }
                    }

                }
        );

    };




    @Override
    protected void finalize() throws Throwable {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mReceiver);
        super.finalize();
    }
    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public Object getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
    /* We inflate the xml which gives us a view */
        if (view == null) {
            view = inflater.inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }

        ColourModel model = (ColourModel) this.getItem(position);

        holder.uiName.setText(model.getColourName());
        holder.uiName.setBackgroundColor(model.getColor());



        return view;
    }
    public ArrayList<ColourModel> getFilteredData() {
        return filteredData;
    }

    static class ViewHolder {
        @Bind(R.id.txtlist_item)
        TextView uiName;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }





}
