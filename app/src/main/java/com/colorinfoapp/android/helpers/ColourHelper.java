package com.colorinfoapp.android.helpers;

import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;

public class ColourHelper {


    public static int colorFromHex(String hex) {
        if (hex.charAt(0) != '#')
            hex = "#" + hex;

        return Color.parseColor(hex);
    }
    public static String hexFromColor(int color) {
        int transparency = Color.alpha(color);

        if (transparency == 0xFF) {
            String hexColor = String.format("#%06X", (0xFFFFFF & color));
            return hexColor;
        }
        String hex =String.format("#%08X", (0xFFFFFFFF & color));
        return hex;
    }


    public static int[] rgbComponents(int colour){
        return new int[]{Color.alpha(colour),Color.red(colour),Color.green(colour),Color.blue(colour)};
    }

    public static int colorFromRgbComponents(int[] components)
    {
        return Color.argb(components[0], components[1], components[2],components[3]);
    }

    public static float [] hsvComponentsFromColor(int color){
        float[] hsv = new float[3];
        Color.colorToHSV(color,hsv);
        return hsv;
    }
    public static int colorFromHsvComponents(float[] hsv){
        int color = Color.HSVToColor(hsv);
        return color;
    }

    public static float[] hslComponentsFromColor(int color){
     float [] hsl = new float[3];
        ColorUtils.colorToHSL(color, hsl);
        return hsl;
    }

    public static int colorFromHslComponents(float[] hsl){
        int color = ColorUtils.HSLToColor(hsl);
        return color;
    }



    public static float[] cmykComponentsFromColor(int color)
    {
        float red = (float)Color.red(color)/255;
        float green = (float)Color.green(color)/255;
        float blue =(float) Color.blue(color)/255;
        float black = 1.0f - (Math.max(Math.max(red, green), blue));

        if (black != 1) {
            float cyan    = (1-red-black)/(1-black);
            float magenta = (1-green-black)/(1-black);
            float yellow  = (1-blue-black)/(1-black);
            return new float[]{cyan,magenta,yellow,black};
        } else {
            float cyan = 1- red;
            float magenta = 1 - green;
            float yellow = 1- blue;
            return new float [] {cyan,magenta,yellow,black};
        }
    }

    public static int colorFromCmykComponents(float[] cmykComponents)
    {
        float cyan = ( cmykComponents[0] * ( 1 - cmykComponents[3] ) + cmykComponents[3] );
        float magenta = ( cmykComponents[1] * ( 1 - cmykComponents[3] ) + cmykComponents[3]);
        float yellow = ( cmykComponents[2] * ( 1 -cmykComponents[3] ) + cmykComponents[3]);

        int red =Math.round (( 1 - cyan ) * 255);
        int green = Math.round (( 1 - magenta ) * 255);
        int blue = Math.round (( 1 - yellow ) * 255);

       return Color.rgb(red, green, blue);
    }

    public static double[] labComponentsFromColor(int color)
    {
        double red = (double)Color.red(color)/255;
        double green = (double)Color.green(color)/255;
        double blue =(double) Color.blue(color)/255;

        if (red > 0.04045)
            red = Math.pow(((red + 0.055)/1.055),2.4);
        else
            red = red/12.92;

        if (green > 0.04045)
            green = Math.pow(((green + 0.055)/1.055),2.4);
        else
            green = green/12.92;

        if (blue > 0.04045)
            blue = Math.pow(((blue + 0.055)/1.055),2.4);
        else
            blue = blue/12.92;

        red = red*100;
        green = green*100;
        blue = blue*100;

        double x = red *0.4124 +green*0.3576+blue*0.1805;
        double y = red*0.2126+green*0.7152+blue*0.0722;
        double z = red*0.0193+green*0.1192+blue*0.9505;

        x = x/95.047;
        y = y/100.0;
        z = z/108.883;

        if (x > 0.008856)
            x = Math.pow(x,1.0/3.0);
        else
            x = (903.3*x+16.0)/116.0;

        if (y > 0.008856)
            y = Math.pow(y,1.0/3.0);
        else
            y = (903.3*y+16.0)/116.0;

        if (z > 0.008856)
            z = Math.pow(z,1.0/3.0);
        else
            z = (903.3*z+16.0)/116.0;

        double l = (116.0*y)-16.0;
        double a = 500.0*(x-y);
        double b = 200.0*(y-z);

        return new double[]{l,a,b};
    }

    public static int colorFromLabComponents(double[] labComponents)
    {

        double y = (labComponents[0] + 16.0 )/116.0;
        double x = (labComponents[1]/ 500.0) + y;
        double z = y - (labComponents[2]/200.0);


        if ( (Math.pow(y,3.0)) > 0.008856 )
            y = Math.pow(y,3.0);
        else
            y = ( y*116.0 - 16.0 ) / 903.3;

        if ( Math.pow(x,3.0) > 0.008856 )
            x = Math.pow(x,3.0);
        else
            x = ( x*116.0 - 16.0 ) / 903.3;
        if ( Math.pow(z,3.0)> 0.008856 )
            z = Math.pow(z,3.0);
        else
            z = ( z*116- 16.0 ) / 903.3;



         double var_x = 95.047*x;
         double var_y = 100.0 *y;
         double var_z = 108.883 *z;

//dovde je ok//

       double  var_X = var_x / 100.0;
        double var_Y = var_y / 100.0;
        double var_Z = var_z / 100.0;


        double red = var_X * 3.24071  +var_Y * (-1.53726) + var_Z * (-0.498571);
        double green = var_X * (-0.969258) + var_Y *  1.87599  + var_Z *  0.0415557 ;
        double blue  = var_X *  0.0556352  + var_Y * (-0.203996) + var_Z *  1.05707 ;

        if ( red > 0.0031308 )
            red = (1.055 * ( Math.pow(red,( 1.0 / 2.4 )))) - 0.055;
        else
            red = 12.92 * red;
        if ( green > 0.0031308 )
            green= (1.055 * ( Math.pow(green ,( 1.0 / 2.4 )))) - 0.055;
        else
           green = 12.92 * green;
        if ( blue > 0.0031308 )
            blue = (1.055 * ( Math.pow(blue,( 1.0 / 2.4 )))) - 0.055;
        else
            blue = 12.92 * blue;

        int R = (int) Math.round(red*255);
        int G = (int) Math.round(green*255);
        int B = (int) Math.round(blue * 255);
 double d = 0.0d;

        return  Color.rgb(R, G, B);
    }


}
