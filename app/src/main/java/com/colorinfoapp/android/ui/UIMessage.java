package com.colorinfoapp.android.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.models.FontHelper;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by bsshe on 3/29/2016.
 */
public class UIMessage extends LinearLayout {


    @Bind(R.id.uiFirstParagraph)
    TextView uiText1;

    @Bind(R.id.uiMiddleParagraph)
    TextView uiText2;

    @Bind(R.id.uiLastParagraph)
    TextView uiText3;
    @Bind(R.id.buttonOK)
    Button btnOK;

    public UIMessage (Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_message, this);
        ButterKnife.bind(this);
        // layout is inflated, assign local variables to components
        btnOK.setTypeface(FontHelper.NotoSansRegular);

        uiText3.setTypeface(FontHelper.NotoSansRegular);
        uiText2.setTypeface(FontHelper.NotoSansRegular);
        uiText1.setTypeface(FontHelper.NotoSansRegular);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.UIMessage,
                0, 0);

        try {
            uiText1.setText(a.getString(R.styleable.UIMessage_labelTextMessage));
            uiText1.setWidth(a.getDimensionPixelSize(R.styleable.UIMessage_labelMessageFontSize, 20));
            uiText1.setTypeface(FontHelper.NotoSansRegular);

            uiText2.setText(a.getString(R.styleable.UIMessage_labelTextMessage));
            uiText2.setWidth(a.getDimensionPixelSize(R.styleable.UIMessage_labelMessageFontSize, 20));
            uiText2.setTypeface(FontHelper.NotoSansRegular);


            uiText3.setText(a.getString(R.styleable.UIMessage_labelTextMessage));
            uiText3.setWidth(a.getDimensionPixelSize(R.styleable.UIMessage_labelMessageFontSize, 20));
            uiText3.setTypeface(FontHelper.NotoSansRegular);


        } finally {
            a.recycle();
        }


    }
}

