package com.colorinfoapp.android.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.models.FontHelper;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by bsshe on 3/29/2016.
 */
public class UIDialog extends LinearLayout {

    @Bind(R.id.uiTitle)
    TextView txtMenu;

    @Bind(R.id.buttonGallery)
    Button btnGallery;

    @Bind(R.id.buttonCamera)
    Button btnCamera;

    @Bind(R.id.buttonClose)
    Button btnClose;

    public UIDialog(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_dialog, this);
        ButterKnife.bind(this);
        // layout is inflated, assign local variables to components
        btnGallery.setTypeface(FontHelper.NotoSansRegular);
        btnCamera.setTypeface(FontHelper.NotoSansRegular);
        btnClose.setTypeface(FontHelper.NotoSansRegular);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.UIDialog,
                0, 0);

        try {
            txtMenu.setText(a.getString(R.styleable.UIDialog_labelTextMenu));
            txtMenu.setWidth(a.getDimensionPixelSize(R.styleable.UIDialog_labelMenuFontSize, 10));
            txtMenu.setTypeface(FontHelper.NotoSansRegular);
        } finally {
            a.recycle();
        }


    }
}
