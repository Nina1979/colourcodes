package com.colorinfoapp.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.helpers.ColourHelper;
import com.colorinfoapp.android.models.FontHelper;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by zvelj on 11/29/2015.
 */
public class UIColorInfoHSV extends LinearLayout {
    @Bind(R.id.txtHelp)
    TextView txtHelp;
    @Bind(R.id.txtHSV)
    TextView txtHSV;
    @Bind(R.id.txtH)
    UIColorValue txtH;
    @Bind(R.id.txtS)
    UIColorValue txtS;
    @Bind(R.id.txtV)
    UIColorValue txtV;

    public UIColorInfoHSV(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.ui_color_info_hsv, this);
        ButterKnife.bind(this);
        txtHSV.setTypeface(FontHelper.NotoSansRegular);
        LocalBroadcastManager.getInstance(context).registerReceiver(mReceiver,
                new IntentFilter("selected-color-changed"));
    }

    @Override
    protected void finalize() throws Throwable {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);
        super.finalize();
    }

    public void ChangeText(boolean swapMode){
        if (swapMode){
            txtHelp.setText("(Swap mode - Hold for edit)");
            txtHelp.setTypeface(FontHelper.NotoSansRegular);}
        else {
            txtHelp.setText("(Edit mode - Hold for swap)");
            txtHelp.setTypeface(FontHelper.NotoSansRegular);
        }
    }

    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int pixel =intent.getIntExtra("selected-color",-1);

            ColourHelper colHelp = new ColourHelper();

            float[] colorArray = new float[2];
            colorArray = colHelp.hsvComponentsFromColor(pixel);

            float HueValue = colorArray[0];
            float hue = Math.round(HueValue);
            String Hue = String.format("%.0f",hue );
            txtH.editColor.setText(Hue);

            float SaturationValue = colorArray[1];
            float saturation = (SaturationValue*100);
            String Saturation = String.format("%.1f", saturation);
            txtS.editColor.setText(Saturation);

            float Value = colorArray[2];
            float v = (Value*1000)/10.0f;
            String value= String.format("%.1f", v);
            txtV.editColor.setText(value);


        }
    };
}