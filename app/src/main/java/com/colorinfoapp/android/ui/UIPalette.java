package com.colorinfoapp.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;
import com.colorinfoapp.android.R;
import com.colorinfoapp.android.adapters.ListAdapter;
import com.colorinfoapp.android.adapters.SpinnerAdapter;
import com.colorinfoapp.android.models.FontHelper;
import com.colorinfoapp.android.models.PaletteModel;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by zvelj on 12/7/2015.
 */
public class UIPalette extends LinearLayout {

    @Bind(R.id.spinner)
    Spinner mySpinner;

    @Bind(R.id.listView)
    ListView myList;

    @Bind(R.id.txtPalette)
    TextView txtPalette;


    SpinnerAdapter spinnerAdapter;
    Context context;
    private ListAdapter listAdapter;
    int selectedColor;


    public UIPalette(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.ui_palette, this);
            ButterKnife.bind(this);
            txtPalette.setTypeface(FontHelper.NotoSansRegular);

            // registering the receiver
        if(!isInEditMode()) {
            LocalBroadcastManager.getInstance(context).registerReceiver(mReceiver,
                    new IntentFilter("selected-color-changed"));

            // Creating adapter for spinner
            this.spinnerAdapter = new SpinnerAdapter(context);
            mySpinner.setAdapter(this.spinnerAdapter);
            //taking the first item-palette from spinner adapter
            PaletteModel selectedModel = (PaletteModel) this.spinnerAdapter.getItem(0);
            // ceating list adapter
            this.listAdapter = new ListAdapter(this.context, selectedModel);
            // setting the list adapter
            myList.setAdapter(listAdapter);
            //
            mySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    // based on selected item we get the palette
                    PaletteModel myPalette = (PaletteModel) UIPalette.this.spinnerAdapter.getItem(position);
                    // calling the method update selected palette  and forwarding the selected palette to list adapter
                    UIPalette.this.listAdapter.updateSelectedPalette(myPalette);
                    listAdapter.refilter(selectedColor);
                    listAdapter.notifyDataSetChanged();


                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {

                }

            });
        }
    }

      // creating the broadcast receiver and getting the selected color from the intent info
       BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            selectedColor= intent.getIntExtra("selected-color", -1);
            listAdapter.refilter(selectedColor);
            listAdapter.notifyDataSetChanged();
            }
        };



        // unregistering the receiver
        @Override
        protected void finalize() throws Throwable {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(mReceiver);
            super.finalize();
        }




    };


