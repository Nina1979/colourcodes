package com.colorinfoapp.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.helpers.ColourHelper;
import com.colorinfoapp.android.models.FontHelper;

import butterknife.Bind;
import butterknife.ButterKnife;

public class UIColorInfoRGB extends LinearLayout {
    @Bind(R.id.txtHelp)
    TextView txtHelp;
    @Bind(R.id.txtRGB)
    TextView txtRGB;
    @Bind(R.id.txtR)
    UIColorValue txtR;
    @Bind(R.id.txtG)
    UIColorValue txtG;
    @Bind(R.id.txtB)
    UIColorValue txtB;
    @Bind(R.id.txtHex)
    UIColorValue txtHex;





    public UIColorInfoRGB(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.ui_color_info_rgb, this);
        ButterKnife.bind(this);

        txtRGB.setTypeface(FontHelper.NotoSansRegular);
        LocalBroadcastManager.getInstance(context).registerReceiver(mReceiver,
                new IntentFilter("selected-color-changed"));

    }

    @Override
    protected void finalize() throws Throwable {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);
        super.finalize();
    }



    public void ChangeText(boolean swapMode) {
        if (swapMode) {
            txtHelp.setText("(Swap mode - Hold for edit)");
            txtHelp.setTextSize(10);
            txtHelp.setTypeface(FontHelper.NotoSansRegular);

        }
        else {
            txtHelp.setText("(Edit mode - Hold for swap)");
            txtHelp.setTextSize(10);
            txtHelp.setTypeface(FontHelper.NotoSansRegular);

        }

    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int pixel =intent.getIntExtra("selected-color",-1);
            ColourHelper colHelp = new ColourHelper();
            int redValue = Color.red(pixel);

            txtR.editColor.setText(String.valueOf(redValue));
            int greenValue = Color.green(pixel);

            txtG.editColor.setText(String.valueOf(greenValue));
            int blueValue = Color.blue(pixel);

            txtB.editColor.setText(String.valueOf(blueValue));

            String hex = colHelp.hexFromColor(pixel);
            String Hex = hex.substring(1);
            txtHex.editColor.setText(Hex);
        }
    };

}
