package com.colorinfoapp.android.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.models.FontHelper;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by zvelj on 11/29/2015.
 */
public class UIColorValue extends LinearLayout {

    @Bind(R.id.txtLabel)
    TextView txtLabel;
    @Bind(R.id.edColor)
    EditText editColor;




    public UIColorValue(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.ui_color_value, this);
        ButterKnife.bind(this);
        // layout is inflated, assign local variables to components


        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.UIColorValue,
                0, 0);

        try {
            txtLabel.setText(a.getString(R.styleable.UIColorValue_labelText));
            txtLabel.setWidth(a.getDimensionPixelSize(R.styleable.UIColorValue_labelFontSize, 30));
            txtLabel.setTypeface(FontHelper.NotoSansRegular);
        } finally {
            a.recycle();
        }


    }


}
