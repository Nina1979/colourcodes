package com.colorinfoapp.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.helpers.ColourHelper;
import com.colorinfoapp.android.models.FontHelper;

import butterknife.Bind;
import butterknife.ButterKnife;


public class UIColorInfoCMYK extends LinearLayout {
    @Bind(R.id.txtHelp)
    TextView txtHelp;
    @Bind(R.id.txtCMYK)
    TextView txtCMYK;
    @Bind(R.id.txtC)
    UIColorValue txtC;
    @Bind(R.id.txtM)
    UIColorValue txtM;
    @Bind(R.id.txtY)
    UIColorValue txtY;
    @Bind(R.id.txtK)
    UIColorValue txtK;

    public UIColorInfoCMYK(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.ui_color_info_cmyk, this);
        ButterKnife.bind(this);
        txtCMYK.setTypeface(FontHelper.NotoSansRegular);
        LocalBroadcastManager.getInstance(context).registerReceiver(mReceiver,
                new IntentFilter("selected-color-changed"));
    }

    @Override
    protected void finalize() throws Throwable {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);
        super.finalize();
    }

    public void ChangeText(boolean swapMode){
        if (swapMode){
            txtHelp.setText("(Swap mode - Hold for edit)");
            txtHelp.setTypeface(FontHelper.NotoSansRegular);}
        else {
            txtHelp.setText("(Edit mode - Hold for swap)");
            txtHelp.setTypeface(FontHelper.NotoSansRegular);
        }
    }

    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int pixel =intent.getIntExtra("selected-color",-1);

            ColourHelper colHelp = new ColourHelper();

            float[] colorArray = new float[3];
            colorArray = colHelp.cmykComponentsFromColor(pixel);

            float cyanValue = colorArray[0];
            String cyan = String.format("%.2f", cyanValue);
            txtC.editColor.setText(cyan);

            float magentaValue = colorArray[1];
            String magenta = String.format("%.2f", magentaValue);
            txtM.editColor.setText(magenta);

            float yellowValue = colorArray[2];
            String yellow = String.format("%.2f", yellowValue);
            txtY.editColor.setText(yellow);

            float blackValue = colorArray[3];
            String black = String.format("%.2f",blackValue );
            txtK.editColor.setText(black);
        }
    };

}
