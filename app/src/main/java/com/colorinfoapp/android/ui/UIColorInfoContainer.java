package com.colorinfoapp.android.ui;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import com.colorinfoapp.android.R;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by zvelj on 12/3/2015.
 */
public class UIColorInfoContainer extends LinearLayout {
    @Bind(R.id.rgb)
    UIColorInfoRGB layoutRGB;
    @Bind(R.id.cmyk)
    UIColorInfoCMYK layoutCMYK;
    @Bind(R.id.hsv)
    UIColorInfoHSV layoutHSV;


    public boolean swapMode = true;

    public UIColorInfoContainer(Context context, final AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.ui_color_info_container, this);
        ButterKnife.bind(this);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (swapMode == false)
                    return;


                if (layoutRGB.getVisibility() == View.VISIBLE) {
                    layoutRGB.setVisibility(View.GONE);
                    layoutCMYK.setVisibility(View.VISIBLE);
                    layoutHSV.setVisibility(View.GONE);

                } else if (layoutCMYK.getVisibility() == View.VISIBLE) {
                    layoutCMYK.setVisibility(View.GONE);
                    layoutRGB.setVisibility(View.GONE);
                    layoutHSV.setVisibility(View.VISIBLE);

                } else {
                    layoutRGB.setVisibility(View.VISIBLE);
                    layoutCMYK.setVisibility(View.GONE);
                    layoutHSV.setVisibility(View.GONE);

                }
                UIColorInfoContainer.this.postInvalidate();
            }
        });

        this.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                swapMode = !swapMode;
                layoutRGB.ChangeText(swapMode);
                layoutCMYK.ChangeText(swapMode);
                layoutHSV.ChangeText(swapMode);

                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (swapMode)
            return true;

        if ((event.getEventTime() - event.getDownTime() > 1200)) {
            this.setFocusable(false);
            swapMode = true;
            layoutRGB.ChangeText(swapMode);
            layoutCMYK.ChangeText(swapMode);
            layoutHSV.ChangeText(swapMode);
            return true;
        }

        return false;
    }



}
