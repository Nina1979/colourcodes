package com.colorinfoapp.android.models;

import android.content.Context;

import com.opencsv.CSVReader;
import com.colorinfoapp.android.R;
import com.colorinfoapp.android.helpers.ColourHelper;

import java.io.IOException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by bsshe on 3/12/2016.
 */
public class ColourData {
    private ArrayList<PaletteModel> palettes = new ArrayList<>();
    private Context context;
    private boolean loadedData;

    private static ColourData instance;
    public static ColourData getInstance() {
        if (instance == null)
            instance = new ColourData();
        return instance;
    }

    private ColourData() {

    }

    public void init(Context context) {
        this.context = context;
    }
// loading data
    public void loadData() {
        if (loadedData)
            return;

        //Reading csv file as string
        PaletteModel pantonePalette = new PaletteModel();
        pantonePalette.setPaletteName("Pantone");
        try {
            InputStream is = context.getResources().openRawResource(R.raw.pantone);
            InputStreamReader csvStreamReader = new InputStreamReader(is);
            CSVReader csvReader = new CSVReader(csvStreamReader);

            // throw away the header
            csvReader.readNext();
            // nextLine[] is an array of values from the line
            String [] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {

                //creating a new instance of color model
                ColourModel colorModel = new ColourModel();
                // getting the name of color model from the next line array and seeting the color name in the color model
                colorModel.setColourName(nextLine[0]);
                // getting the r, g, and b component from the next line array and converting them from string to integer values
                int R = Integer.valueOf(nextLine[1]);
                int G = Integer.valueOf(nextLine[2]);
                int B = Integer.valueOf(nextLine[3]);
                //setting the color which we got from r,g and b components
                colorModel.setColor(ColourHelper.colorFromRgbComponents(new int[]{255,R,G,B}));
                //adding the color model into arraylist
                pantonePalette.getColours().add(colorModel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //adding the palette model into arraylist
        this.getPalettes().add(pantonePalette);

        //Reading resene csv file as string
        PaletteModel resenePalette = new PaletteModel();
        resenePalette.setPaletteName("Resene");
        try {
            InputStream is = context.getResources().openRawResource(R.raw.resene);
            InputStreamReader csvStreamReader = new InputStreamReader(is);
            CSVReader csvReader = new CSVReader(csvStreamReader);

            // throw away the header
            csvReader.readNext();

            String [] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {
                // nextLine[] is an array of values from the line

                ColourModel colorModel = new ColourModel();
                colorModel.setColourName(nextLine[0]);
                int R = Integer.valueOf(nextLine[1]);
                int G = Integer.valueOf(nextLine[2]);
                int B = Integer.valueOf(nextLine[3]);

                colorModel.setColor(ColourHelper.colorFromRgbComponents(new int[]{255,R,G,B}));
                resenePalette.getColours().add(colorModel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.getPalettes().add(resenePalette);

       //Reading federal standard csv file as string
        PaletteModel federalPalette = new PaletteModel();
        federalPalette.setPaletteName("Federal Standard");

        try {
            InputStream is = context.getResources().openRawResource(R.raw.federal);
            InputStreamReader csvStreamReader = new InputStreamReader(is);
            CSVReader csvReader = new CSVReader(csvStreamReader);

            // throw away the header
            csvReader.readNext();

            String [] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {
                // nextLine[] is an array of values from the line

                ColourModel colorModel = new ColourModel();
                colorModel.setColourName(nextLine[0]);
                int R = Integer.valueOf(nextLine[1]);
                int G = Integer.valueOf(nextLine[2]);
                int B = Integer.valueOf(nextLine[3]);

                colorModel.setColor(ColourHelper.colorFromRgbComponents(new int[]{255,R,G,B}));
                federalPalette.getColours().add(colorModel);
            }
        } catch (IOException e) {

            e.printStackTrace();
        }
        this.getPalettes().add(federalPalette);


        //Reading ral csv file as string
        PaletteModel ralPalette = new PaletteModel();
        ralPalette.setPaletteName("Ral");
        try {
            InputStream is = context.getResources().openRawResource(R.raw.ral);
            InputStreamReader csvStreamReader = new InputStreamReader(is);
            CSVReader csvReader = new CSVReader(csvStreamReader);

            // throw away the header
            csvReader.readNext();

            String [] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {
                // nextLine[] is an array of values from the line

                ColourModel colorModel = new ColourModel();
                colorModel.setColourName(nextLine[0]);
                int R = Integer.valueOf(nextLine[1]);
                int G = Integer.valueOf(nextLine[2]);
                int B = Integer.valueOf(nextLine[3]);

                colorModel.setColor(ColourHelper.colorFromRgbComponents(new int[]{255,R,G,B}));
                ralPalette.getColours().add(colorModel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.getPalettes().add(ralPalette);

        //Reading british standard csv file as string
        PaletteModel britishPalette = new PaletteModel();
        britishPalette.setPaletteName("British Standard");
        try {
            InputStream is = context.getResources().openRawResource(R.raw.british);
            InputStreamReader csvStreamReader = new InputStreamReader(is);
            CSVReader csvReader = new CSVReader(csvStreamReader);

            // throw away the header
            csvReader.readNext();

            String [] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {
                // nextLine[] is an array of values from the line

                ColourModel colorModel = new ColourModel();
                colorModel.setColourName(nextLine[0]);
                int R = Integer.valueOf(nextLine[1]);
                int G = Integer.valueOf(nextLine[2]);
                int B = Integer.valueOf(nextLine[3]);

                colorModel.setColor(ColourHelper.colorFromRgbComponents(new int[]{255,R,G,B}));
                britishPalette.getColours().add(colorModel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.getPalettes().add(britishPalette);

        loadedData = true;
    }


    public ArrayList<PaletteModel> getPalettes() {
        return palettes;
    }


}