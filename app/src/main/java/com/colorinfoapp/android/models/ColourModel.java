package com.colorinfoapp.android.models;

/**
 * Created by bsshe on 3/12/2016.
 */
public class ColourModel {
    private String colourName;
    private int color;

    public ColourModel () {

    }

    public String getColourName() {
        return colourName;
    }

    public  void setColourName(String colourName) {
        this.colourName = colourName;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
