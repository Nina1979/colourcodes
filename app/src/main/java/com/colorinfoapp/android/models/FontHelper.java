package com.colorinfoapp.android.models;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by bsshe on 3/29/2016.
 */
public class FontHelper {
    public static Typeface NotoSansRegular;

    public static void initialize(Context context) {
        NotoSansRegular = Typeface.createFromAsset(context.getAssets(), "fonts/NotoSans-Regular.ttf");
    }
}
