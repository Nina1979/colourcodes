package com.colorinfoapp.android.models;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by bsshe on 3/17/2016.
 */
public class PaletteModel {
    private String paletteName;
    private ArrayList<ColourModel> colours = new ArrayList<ColourModel>();
    private Context context;
    public PaletteModel() {

    }
    private static PaletteModel instance;
    public static PaletteModel getInstance() {
        if (instance == null)
            instance = new PaletteModel();
        return instance;
    }
    public String getPaletteName() {
        return paletteName;
    }

    public void setPaletteName(String paletteName) {
        this.paletteName = paletteName;
    }

    public ArrayList<ColourModel> getColours() {

        return colours;
    }


}



