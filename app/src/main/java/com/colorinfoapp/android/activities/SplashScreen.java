package com.colorinfoapp.android.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.models.FontHelper;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by zvelj on 11/25/2015.
 */
public class SplashScreen extends AppCompatActivity {
    private Typeface font;
    Context context;
    @Bind(R.id.txtSplash)TextView tv1;
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        ButterKnife.bind(this);

        tv1.setTypeface(FontHelper.NotoSansRegular);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
// close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);

    }
}
