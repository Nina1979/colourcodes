package com.colorinfoapp.android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.colorinfoapp.android.R;
import com.colorinfoapp.android.models.FontHelper;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by bsshe on 3/28/2016.
 */
public class DialogActivity extends Activity {
    public static final int DIALOG_RESULT_GALLERY_SELECTED = 1;
    public static final int DIALOG_RESULT_CAMERA_SELECTED = 2;


    @Bind(R.id.uiTitle)
    TextView uiText;

    @Bind(R.id.buttonGallery)
    Button btnGallery;

    @Bind(R.id.buttonCamera)
    Button btnCamera;

    @Bind(R.id.buttonClose)
    Button btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this);



        uiText.setTypeface(FontHelper.NotoSansRegular);

        btnGallery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//
                setResult(DIALOG_RESULT_GALLERY_SELECTED);
                finish();
            }
        });

        btnCamera.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                setResult(DIALOG_RESULT_CAMERA_SELECTED);
                finish();
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                finish();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


    }


}
