package com.colorinfoapp.android.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.models.FontHelper;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by ninat on 7/7/2016.
 */
public class MessageActivity extends Activity {

    @Bind(R.id.uiFirstParagraph)
    TextView uiText1;

    @Bind(R.id.uiMiddleParagraph)
    TextView uiText2;

    @Bind(R.id.uiLastParagraph)
    TextView uiText3;
    @Bind(R.id.buttonOK)
    Button btnOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ButterKnife.bind(this);

        uiText1.setTypeface(FontHelper.NotoSansRegular);
        uiText2.setTypeface(FontHelper.NotoSansRegular);
        uiText3.setTypeface(FontHelper.NotoSansRegular);
        btnOK.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                finish();
            }
        });
    }



}
