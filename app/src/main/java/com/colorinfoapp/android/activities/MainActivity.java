package com.colorinfoapp.android.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.colorinfoapp.android.R;
import com.colorinfoapp.android.models.FontHelper;
import com.colorinfoapp.android.ui.ScrollImageView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

import com.google.android.gms.ads.AdRequest;


public class MainActivity extends AppCompatActivity {
    public static final int ACTIVITY_IMAGE_CAPTURE_CODE = 1;
    public static final int ACTIVITY_DIALOG_CODE = 2;
    public static final int ACTIVITY_GALLERY_CODE = 3;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA_CODE = 4;
    public static final int MY_PERMISSIONS_REQUEST_STORAGE_CODE = 5;
    final String PREFS_NAME = "MyPrefsFile";
    SharedPreferences settings;

    @Bind(R.id.infoControl)
    RelativeLayout infoControl;

    @Bind(R.id.imgPicture)
    ScrollImageView scrollImageView;

    @Bind(R.id.colorInfoContainer)
    LinearLayout colorInfoContainer;

    @Bind(R.id.Container)
    LinearLayout Container;
    @Bind(R.id.Pallete)
    LinearLayout uiPalette;


    int imageWidth;
    int imageHeight;
    Bitmap fullImage;

    @Bind(R.id.txt_picture)
    TextView txtPicture;
    @Bind(R.id.txt_bottom)
    TextView txtAd;
    String pictureImagePath = "";
    InterstitialAd mInterstitialAd;
    Calendar cal;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        scrollImageView.setImage(null);
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        Log.d("Color Info", "***Retaining Custom Non Configuration Instance");
        Log.d("Color Info", String.format("Full image is %s", fullImage == null ? "null" : "not null"));
        if (fullImage != null) {
            Log.d("Color Info", String.format("Full image is %s", fullImage.isRecycled() ? " recycled" : "not recycled"));
        }
        return fullImage;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        fullImage = (Bitmap) getLastCustomNonConfigurationInstance();
        Log.d("Color Info", "***MainActivity onCreate");
        Log.d("Color Info", String.format("Full image is %s", fullImage == null ? "null" : "not null"));
        if (fullImage != null) {
            Log.d("Color Info", String.format("Full image is %s", fullImage.isRecycled() ? " recycled" : "not recycled"));
        }

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-3940256099942544~3347511713");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();

                mInterstitialAd.show();

            }


            @Override
            public void onAdClosed() {

            }
        });

        txtPicture.setTypeface(FontHelper.NotoSansRegular);
        txtAd.setTypeface(FontHelper.NotoSansRegular);
        infoControl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                startCameraActivity();
            }
        });

        infoControl.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent i = new Intent(MainActivity.this, DialogActivity.class);
                startActivityForResult(i, ACTIVITY_DIALOG_CODE);
                return true;
            }

        });

        colorInfoContainer.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // call the listener only once
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    colorInfoContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    colorInfoContainer.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
//                getting the height of the colorInfoContainer and seeting that height to palette view
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) uiPalette.getLayoutParams();
                lp.height = colorInfoContainer.getMeasuredHeight();
                uiPalette.setLayoutParams(lp);
            }
        });



        infoControl.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Log.d("Color Info", "***infoControl OnGlobalLayoutListener");
                // new info control width and height after orientation changed
                int w = infoControl.getWidth();
                int h = infoControl.getHeight();
                // checking if old width and height are different than the new ones
                if (imageWidth != w || imageHeight != h) {
                    Log.d("Color Info", String.format("ow:%d  oh:%d  nw:%d nh:%d", imageWidth, imageHeight, w, h));

                    imageWidth = w;
                    imageHeight = h;

                    if (fullImage != null && !fullImage.isRecycled()) {
                        Log.d("Color Info", "updateImage");
                        updateImage();
                   }
//                    infoControl.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        // calling the update scrollImageView method to calculate the new scale size
//
                    }
                }
            }

            );

        }

        @Override
        public void onBackPressed () {
            if (scrollImageView.getImage() != null) {
                scrollImageView.getImage().recycle();
                scrollImageView.setImage(null);
            }
            scrollImageView.setVisibility(View.GONE);
            if (fullImage != null) {
                fullImage.recycle();
                fullImage = null;
            }
            System.gc();
        }


        @Override
        public void onRequestPermissionsResult ( int requestCode, String permissions[],
        int[] grantResults){
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_CAMERA_CODE: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        startCameraActivity();
                    }
                }
                case MY_PERMISSIONS_REQUEST_STORAGE_CODE: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        startCameraActivity();
                    }
                }
            }
        }


        // request je kod koji ide sa intentom a resultcode je ono sto se vraca iz startactivity

    public void onActivityResult(int requestcode, int resultcode, Intent intent) {
        switch (requestcode) {
            case ACTIVITY_IMAGE_CAPTURE_CODE: {
                try {
                    fullImage = rotateImageIfRequired();
                    updateImage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            case ACTIVITY_DIALOG_CODE:
                if (resultcode == DialogActivity.DIALOG_RESULT_GALLERY_SELECTED)
                    openGalleryActivity(); //call gallery selected function
                if (resultcode == DialogActivity.DIALOG_RESULT_CAMERA_SELECTED)
                    startCameraActivity(); //call  camera selected function

                break;
            case ACTIVITY_GALLERY_CODE:
                if (resultcode == RESULT_OK && null != intent) {
                    try {
                        openImage(intent);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    //The method that will check the current scrollImageView orientation to decide the rotation angle
    private Bitmap rotateImageIfRequired() throws IOException {
        File imgFile = new File(pictureImagePath);
        Bitmap img = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        ExifInterface ei = new ExifInterface(imgFile.getAbsolutePath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    //rotation method
    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    // opening the gallery
    private void openGalleryActivity() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        this.startActivityForResult(i, ACTIVITY_GALLERY_CODE);
    }


    // opening the scrollImageView
    private void openImage(Intent data) throws IOException {
        Uri selectedImage = data.getData();
        InputStream input = this.getContentResolver().openInputStream(selectedImage);
        //ucitavanje velicine slike
        fullImage = BitmapFactory.decodeStream(input, null, null);
        if (input != null) {
            updateImage();
        }
    }

    public void updateImage() {
        int height = fullImage.getHeight();
        int width = fullImage.getWidth();
        Double scaledSize = calculateScaledSize(width, height);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(fullImage, (int) (width * scaledSize), (int) (height * scaledSize), true);
        scrollImageView.setImage(scaledBitmap);
        scrollImageView.setVisibility(View.VISIBLE);
    }

    private double calculateScaledSize(int originalWidth, int originalHeight) {
        double scaledHorizontalMin = (double) imageWidth / originalWidth;
        double scaledVerticalMin = (double) imageHeight / originalHeight;
        double scaledBest = 2 * Math.sqrt((imageWidth * imageHeight) / (originalWidth * originalHeight));
        return Math.max(Math.max(scaledHorizontalMin, scaledVerticalMin), scaledBest);
    }

    private void startCameraActivity() {
        int permissionCheck = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA_CODE);
            return;
        }

        int permissionStorageCheck = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionStorageCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_STORAGE_CODE);
            return;
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = timeStamp + ".jpg";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
        File file = new File(pictureImagePath);
        Uri outputFileUri = Uri.fromFile(file);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(cameraIntent, 1);
    }

    //
    @Override
    protected void onStart() {
        super.onStart();

        settings = getSharedPreferences(PREFS_NAME, 0);

        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy H:m:s", Locale.ENGLISH);
        cal = GregorianCalendar.getInstance();

        try {
            Date currentDate = cal.getTime();
            String currentDateAsString = formatter.format(currentDate);

            String lastShownAtString = settings.getString("ad_shown_on", "");
            if (lastShownAtString.equals("")) {
                settings.edit().putString("ad_shown_on", currentDateAsString).apply();
                lastShownAtString = currentDateAsString;
            }
            Date lastShownDate = formatter.parse(lastShownAtString);

            long diff = currentDate.getTime() - lastShownDate.getTime();
            if (diff > 2 * 24 * 60 * 60 * 1000) { // 2 days
                settings.edit().putString("ad_shown_on", currentDateAsString).apply();
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                        .build();

                mInterstitialAd.loadAd(adRequest);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            Date currentDate = cal.getTime();
            String currentDateAsString = formatter.format(currentDate);
            settings.edit().putString("ad_shown_on", currentDateAsString).apply();
        }

        if (settings.getBoolean("my_first_time", true)) {
            //the app is being launched for first time
            Intent intent = new Intent(this, MessageActivity.class);
            startActivity(intent);
            settings.edit().putBoolean("my_first_time", false).apply();
        }
    }
}