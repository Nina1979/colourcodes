package com.colorinfoapp.android.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.colorinfoapp.android.R;

import butterknife.ButterKnife;

/**
 * Created by ninat on 8/4/2016.
 */
public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);

    }
}
