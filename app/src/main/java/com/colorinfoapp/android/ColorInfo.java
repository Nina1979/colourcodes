package com.colorinfoapp.android;

import android.app.Application;
import android.content.res.Configuration;

import com.colorinfoapp.android.models.ColourData;
import com.colorinfoapp.android.models.FontHelper;

public class ColorInfo extends Application {

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // initializing the method of font helper class
        FontHelper.initialize(this);
        //initializing colour data and loading data
        ColourData.getInstance().init(this);
        ColourData.getInstance().loadData();


    }





    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}